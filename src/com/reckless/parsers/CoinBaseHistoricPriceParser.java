package com.reckless.parsers;

import com.reckless.historical.data.domain.PriceRecord;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CoinBaseHistoricPriceParser implements Parser<PriceRecord>{
    @Override
    /** Date,Open,High,Low,Close */
    public PriceRecord parse(String string) {
        String[] parts = string.split(",");
        LocalDateTime time = LocalDateTime.parse(parts[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        LocalDateTime timeFrom = time;
        LocalDateTime timeTo =time.plusMinutes(15);
        BigDecimal openPrice = new BigDecimal(parts[1]);
        BigDecimal maxPrice = new BigDecimal(parts[2]);
        BigDecimal minPrice = new BigDecimal(parts[3]);
        BigDecimal closePrice = new BigDecimal(parts[4]);
        PriceRecord record = PriceRecord.createPriceRecord(timeFrom, timeTo, openPrice, closePrice, maxPrice, minPrice);
        return record;
    }
}
