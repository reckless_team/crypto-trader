package com.reckless.parsers;

public interface Parser <T> {
    T parse (String string);
}
