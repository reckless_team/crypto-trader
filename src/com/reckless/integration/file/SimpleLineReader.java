package com.reckless.integration.file;

import com.reckless.historical.data.domain.PriceRecord;
import com.reckless.parsers.Parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class SimpleLineReader {
   private Stream<String> stringStream;



    public static SimpleLineReader  create (String filePath){
       SimpleLineReader reader = new SimpleLineReader();
       try {
           Path path = Paths.get(filePath);
           reader.stringStream = Files.lines(path);
       } catch (IOException e) {
           e.printStackTrace();
       }
       return  reader;
    }

    public <T> Collection<T>  readWithParser(Parser<T> parser){
        List<T> list = new ArrayList();
        stringStream.forEach(s-> parseAndAddToList(s,list,parser));
        return  list;
    }

    private <T> void parseAndAddToList(String s, List<T> list, Parser<T> parser) {
        T element = parser.parse(s);
        System.out.println(element);
        list.add(element);
    }
}
