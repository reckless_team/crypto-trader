package com.reckless.domain;

import java.math.BigDecimal;

public class Amount {
    private final BigDecimal value;

    public Amount(String value) {
        this.value = new BigDecimal(value);
    }

    public static Amount of(String amount) {
        return new Amount(amount);
    }
}
