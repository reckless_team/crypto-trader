package com.reckless.domain;

import java.math.BigDecimal;

public class Quote {
	
	private BigDecimal price;
	private Long timestamp;

	public Quote at (Long timestamp){
		this.timestamp=timestamp;
		return this;
	}
	public static Quote of(String value){
		Quote quote = new Quote();
		quote.price = new BigDecimal(value);
		return quote;
	}
	
	
}
