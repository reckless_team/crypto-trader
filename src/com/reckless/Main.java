package com.reckless;


import com.reckless.connectivity.Exchages;
import com.reckless.domain.Amount;
import com.reckless.domain.Asset;
import com.reckless.historical.data.samples.TestHistoricMarketData;
import com.reckless.model.ActionStrategy;
import com.reckless.model.Exchange;
import com.reckless.model.Trader;
import com.reckless.model.basic.MyStupidStrategy;

import static com.reckless.model.basic.SimpleTrader.TraderBuilder.*;

public class Main {

    public static void main(String[] args) {
        TestHistoricMarketData testHistoricMarketData = new TestHistoricMarketData();
        testHistoricMarketData.init();

        Amount ten = Amount.of("10");
        Asset dollars = Asset.in("USD");
        Exchange coinBase = Exchages.obtainForId("coinbase");
        ActionStrategy myStupidStrategy = MyStupidStrategy.create();
        Trader trader = trader().with(ten).of(dollars).using(myStupidStrategy).sendTo(coinBase);

        testHistoricMarketData.subscribe(trader);
    }
}
