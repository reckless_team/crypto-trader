package com.reckless.historical.data.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class PriceRecord {
    private LocalDateTime timeFrom;
    private LocalDateTime timeTo;
    private BigDecimal openPrice;
    private BigDecimal closePrice;
    private BigDecimal maxPrice;
    private BigDecimal minPrice;


    private PriceRecord(LocalDateTime timeFrom, LocalDateTime timeTo, BigDecimal openPrice, BigDecimal closePrice, BigDecimal maxPrice, BigDecimal minPrice) {
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.openPrice = openPrice;
        this.closePrice = closePrice;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
    }

    public static PriceRecord createPriceRecord(LocalDateTime timeFrom, LocalDateTime timeTo, BigDecimal openPrice, BigDecimal closePrice, BigDecimal maxPrice, BigDecimal minPrice) {
        return new PriceRecord(timeFrom, timeTo, openPrice, closePrice, maxPrice, minPrice);
    }

    @Override
    public String toString() {
        return "PriceRecord{" +
                "timeFrom=" + timeFrom +
                ", timeTo=" + timeTo +
                ", openPrice=" + openPrice +
                ", closePrice=" + closePrice +
                ", maxPrice=" + maxPrice +
                ", minPrice=" + minPrice +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceRecord that = (PriceRecord) o;
        return timeFrom == that.timeFrom &&
                timeTo == that.timeTo &&
                Objects.equals(openPrice, that.openPrice) &&
                Objects.equals(closePrice, that.closePrice) &&
                Objects.equals(maxPrice, that.maxPrice) &&
                Objects.equals(minPrice, that.minPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeFrom, timeTo, openPrice, closePrice, maxPrice, minPrice);
    }
}
