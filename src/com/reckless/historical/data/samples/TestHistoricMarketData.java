package com.reckless.historical.data.samples;

import com.reckless.historical.data.domain.PriceRecord;
import com.reckless.integration.file.SimpleLineReader;
import com.reckless.model.MarketDataStream;
import com.reckless.model.Trader;
import com.reckless.parsers.CoinBaseHistoricPriceParser;
import com.reckless.parsers.Parser;

import java.util.Collection;

public class TestHistoricMarketData  implements MarketDataStream{

    public void init(){
        String filePath = "/Users/vadik/Documents/projects/java-workspace/BitcoinData/src/resources/coindesk-bpi-USD-ohlc_data-2017-08-19_2017-08-20.csv";
        SimpleLineReader simpleLineReader = SimpleLineReader.create(filePath);
        Parser<PriceRecord> parser = new CoinBaseHistoricPriceParser();
        Collection<PriceRecord> priceRecords = simpleLineReader.readWithParser(parser);
        System.out.println("Read records: "+priceRecords.size());
    }

    @Override
    public void subscribe(Trader trader) {
    }
}
