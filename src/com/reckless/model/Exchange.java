package com.reckless.model;

import com.reckless.domain.BuyOrder;
import com.reckless.domain.SellOrder;

public interface Exchange {

    /** will be just submitOrder in practice. Just sendTo make it clear for now*/

    public void submitSellOrder( BuyOrder order);
    public void submitBuyOrder( SellOrder order);
}
