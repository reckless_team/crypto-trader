package com.reckless.model;

public interface MarketDataStream {
    void subscribe(Trader trader);
}
