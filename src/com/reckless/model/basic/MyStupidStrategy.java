package com.reckless.model.basic;

import com.reckless.domain.Amount;
import com.reckless.domain.Price;
import com.reckless.model.Action;
import com.reckless.model.ActionStrategy;

public class MyStupidStrategy implements ActionStrategy<PriceChangeEvent,Action> {
    /** Generated Strategy for term {day,hour} based on [historic data]*/
    private Price expectedMaximumPrice;
    private Price expectedMinimumPrice;

    private Price targetSellPrice;
    private Price limitSafePrice;
    private Price initiationPriceLimit;

    /** TODO implement state machine:
     * (0) trader just started, and didn't trade yet
     *     buy price = expectedMaximumPrice+expectedMaximumPrice/2 + multiDayMarketMovement ()
     *
     *     current price > buy price                              #then DO NOTHING,wait market to go DOWN
     *     current price < buy price                              #then  BUY


     *
     *
     * (1)
     * if current price < limitSafePrice                           #then  SELL, even we lose - it's unpredicted event
     *
     * (2)
     * if current price < initialTradePrice                        #then DO NOTHING, we'll wait for market to go up
     *
     * (3)
     * if current price > tradedPrice
     *                  and
     *    current price < targetSellPrice                          #then DO NOTHING, we'll wait for market move up a bit more
     *
     * (4)
     *  if current price
     *
     * */

    @Override
    public Action decideAction(PriceChangeEvent event) {
        Amount currentAmmount = event.getCurrentAmmount();
        Price previousTradePrice = event.getPreviousTradePrice();


        Action action = null;
        return action;
    }



    public static ActionStrategy create() {
        //TODO initialize fields here
        return  new MyStupidStrategy();
    }
}
