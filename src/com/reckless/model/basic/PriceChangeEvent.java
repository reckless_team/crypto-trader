package com.reckless.model.basic;

import com.reckless.domain.Amount;
import com.reckless.domain.Position;
import com.reckless.domain.Price;
import com.reckless.model.Event;

public class PriceChangeEvent implements Event {

    public Position getCurrentPosition() {
        return null;
    }

    public Amount getCurrentAmmount() {
        return null;
    }

    public Price getPreviousTradePrice() {
        return null;
    }
}
