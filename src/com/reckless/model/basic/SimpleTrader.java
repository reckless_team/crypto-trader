package com.reckless.model.basic;

import com.reckless.domain.*;
import com.reckless.historical.data.domain.HistoricalQuote;
import com.reckless.model.ActionStrategy;
import com.reckless.model.Exchange;
import com.reckless.model.Trader;

public class SimpleTrader implements Trader {

    private final Asset initialAsset;
    private final Amount initialAmount;

    /** Current position data*/
    private Time lastTradeTimestamp;
    private Asset asset;
    private Amount amount;
    private Price tradePrice;

    private final ActionStrategy strategy;


    private SimpleTrader(TraderBuilder traderBuilder) {
        this.initialAmount = traderBuilder.amount;
        this.initialAsset = traderBuilder.asset;
        this.strategy = traderBuilder.strategy;
        this.amount = traderBuilder.amount;
        this.asset = traderBuilder.asset;
    }
    @Override
    public void onMarketDataEvent(MarketData marketData){
        PriceChangeEvent event = null;
    }


    public static class TraderBuilder{

        private Amount amount;
        private ActionStrategy strategy;
        private Exchange exchange;
        private Asset asset;

        public TraderBuilder with(Amount amount){
           this.amount = amount;
           return  this;
        }
        public TraderBuilder of(Asset asset){
            this.asset = asset;
            return  this;
        }

        public TraderBuilder using(ActionStrategy strategy ){
           this.strategy = strategy;
           return this;
        }

        public Trader sendTo(Exchange exchange){
           this.exchange = exchange;
           return new SimpleTrader(this);
        }

        public static TraderBuilder trader(){
           return  new TraderBuilder();
        }
    }

}
