package com.reckless.model;

public interface ActionStrategy <E extends  Event, A extends Action> {

    A decideAction(E event);
}
