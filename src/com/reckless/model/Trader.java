package com.reckless.model;

import com.reckless.domain.MarketData;

public interface Trader {
    void onMarketDataEvent(MarketData marketData);
}
